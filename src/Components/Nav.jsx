import {Link,useNavigate} from 'react-router-dom'
import storage from '../Storage/storage'

const Nav = () => {
  return (
    <nav className='navbar navbar-expand-lg navbar-white bg-success'>
      <div className='container-fluid'>
        <a className='navbar-brand h1' >Heroes de Marvel</a>
        <button className='navbar-toggler' type='button' data-bs-toggle='collapse' 
        data-bs-target='#nav' aria-controls='navbarSupportedContent'>
          <span className='navbar-toggler-icon'></span>

        </button>
      </div>
    </nav>
  )
}

export default Nav